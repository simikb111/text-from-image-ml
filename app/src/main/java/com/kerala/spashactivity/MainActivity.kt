package com.kerala.spashactivity

import android.Manifest

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextDetector
import kotlinx.android.synthetic.main.activity_splash.*
//import com.google.firebase.ml.vision.text.FirebaseVisionTextDetector;

import java.io.File

class MainActivity : AppCompatActivity(), View.OnClickListener {

   /*
    private val myImageView: ImageView? = null
    private val myTextView: TextView? = null*/
    val WRITE_STORAGE = 100
    val SELECT_PHOTO = 102
    var photo: File? = null
    private var myBitmap: Bitmap? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
       /* myTextView = findViewById(R.id.textView);
        myImageView = findViewById(R.id.imageView);*/
        checkText.setOnClickListener(this);
        select_image.setOnClickListener(this);
        Glide.with(this)
            .load(R.raw.ml3)
            .into(myImageView);

    }


    override fun onClick(view: View) {
        when (view.getId()) {
            R.id.checkText -> if (myBitmap != null) {
                runTextRecognition()
            }
            R.id.select_image -> checkPermission(WRITE_STORAGE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                WRITE_STORAGE -> checkPermission(requestCode)
                SELECT_PHOTO -> {
                    val dataUri: Uri? = data!!.data
                    val path = CommonUtils.getPath(this, dataUri)
                    if (path == null) {
                        myBitmap = CommonUtils.resizePhoto(photo, this, dataUri, myImageView)
                    } else {
                        myBitmap = CommonUtils.resizePhoto(photo, path, myImageView)
                    }
                    if (myBitmap != null) {
                        myTextView.setText(null)
                        myImageView.setImageBitmap(myBitmap)
                    }
                }
            }
        }
    }


    private fun runTextRecognition() {
        val image = myBitmap?.let { FirebaseVisionImage.fromBitmap(it) }
        val detector: FirebaseVisionTextDetector =
            FirebaseVision.getInstance().getVisionTextDetector()
        image?.let {
            detector.detectInImage(it)
                .addOnSuccessListener(OnSuccessListener<FirebaseVisionText?> { texts ->
                    processExtractedText(
                        texts!!
                    )
                }).addOnFailureListener(
                    OnFailureListener {
                        Toast.makeText(
                            this@MainActivity,
                            "Exception", Toast.LENGTH_LONG
                        ).show()
                    })
        }
    }

    private fun processExtractedText(firebaseVisionText: FirebaseVisionText) {
        myTextView.setText(null)
        if (firebaseVisionText.getBlocks().size === 0) {
            myTextView.setText(R.string.no_text)
            return
        }
        for (block in firebaseVisionText.getBlocks()) {
            myTextView.append(block.getText())
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            WRITE_STORAGE ->
                //If the permission request is granted, then...//
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //...call selectPicture//
                    selectPicture()
                    //If the permission request is denied, then...//
                } else {
                    //...display the “permission_request” string//
                    requestPermission(this, requestCode, R.string.permission_request)
                }
        }
    }

    //Display the permission request dialog//
    fun requestPermission(activity: Activity, requestCode: Int, msg: Int) {
        val alert: AlertDialog.Builder = AlertDialog.Builder(this@MainActivity)
        alert.setMessage(msg)
        alert.setPositiveButton("yes",
            DialogInterface.OnClickListener { dialogInterface, i ->
                dialogInterface.dismiss()
                val permissonIntent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                permissonIntent.data = Uri.parse("package:" + activity.packageName)
                activity.startActivityForResult(permissonIntent, requestCode)
            })
        alert.setNegativeButton("cancel",
            DialogInterface.OnClickListener { dialogInterface, i -> dialogInterface.dismiss() })
        alert.setCancelable(false)
        alert.show()
    }

    //Check whether the user has granted the WRITE_STORAGE permission//
    fun checkPermission(requestCode: Int) {
        when (requestCode) {
            WRITE_STORAGE -> {
                val hasWriteExternalStoragePermission = ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )

                //If we have access to external storage...//
                if (hasWriteExternalStoragePermission == PackageManager.PERMISSION_GRANTED) {
                    //...call selectPicture, which launches an Activity where the user can select an image//
                    selectPicture()
                    //If permission hasn’t been granted, then...//
                } else {
                    //...request the permission//
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        requestCode
                    )
                }
            }
        }
    }
    private fun selectPicture() {
        photo = CommonUtils.createTempFile(photo)
        val intent =
            Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        //Start an Activity where the user can choose an image//
        startActivityForResult(intent, SELECT_PHOTO)
    }
    override fun onBackPressed() {



        val dialog = Dialog(this@MainActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_exit)

        val text = dialog.findViewById(R.id.text_dialog) as TextView
        text.text = "Do you want to Exit?"

        val dialogButton = dialog.findViewById(R.id.btn_dialog) as Button
        dialogButton.setOnClickListener {

            super.onBackPressed();
            this@MainActivity.finish()

            dialog.dismiss()

        }
        // super.onBackPressed();
        dialog.show()
    }
}
package com.kerala.spashactivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_splash.*

class Splash : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Glide.with(this)
            .load(R.raw.ml2)
            .into(imageView);
        Handler().postDelayed(Runnable {

            startActivity(Intent(this@Splash,MainActivity::class.java))
            finish()
        },3000)
    }
}